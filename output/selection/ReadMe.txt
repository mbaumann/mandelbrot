Parameter set for mandelbrot_20210214-180041.png
------------------------------------------------
width = 0.0082
height = 0.0082
center_real = -0.7718
center_imag = -0.1137

computations = 100e6

color_scheme = "blue"


Parameter set for mandelbrot_20210214-200322.png
------------------------------------------------
width = 3
height = 3
center_real = -0.75
center_imag = 0

computations = 100e6

color_scheme = "green"