"""Setup for c-compiling mandelbrot iteration function
"""
from setuptools import setup
from Cython.Build import cythonize

setup(ext_modules=cythonize("mandelbrot.pyx"))
