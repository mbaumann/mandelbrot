"""Compute and show mandelbrot set
"""

# python3 setup.py build_ext --inplace

import json
import os
import multiprocessing
import itertools
import time
import tqdm
import numpy as np
from PIL import Image

import mandelbrot


def get_color(n_iter, style):
    """Get color value for value of iteration

    Args:
        n_iter (int): number of iteration

    Returns:
        3-tuple: rgb-value
    """
    if style == "bw":
        if n_iter >= 1e3:
            return (0, 0, 0)
        return (255, 255, 255)

    if style == "blackblue":
        if n_iter > 64:
            return (0, 0, 0)  #(255, 255, 255)
        scale = [0, 64, 128, 196]
        b = scale[n_iter % 4]
        g = scale[(n_iter // 4) % 4]
        r = scale[(n_iter // 16) % 4]
        return (r, g, b)

    if style == "stripes":
        if n_iter % 2 == 0 or n_iter == np.inf:
            return (0, 0, 0)
        return (255, 255, 255)

    if style == "green":
        maxiter = 255
        if n_iter == np.inf or n_iter >= maxiter:
            return (0, 0, 0)
        lim = 255 #100
        if n_iter > lim:
            return (255, 255, 255)

        ratio = n_iter / lim
        color_change_ratio_low = 0.3
        color_change_ratio_high = 0.4
        threshold = 215
        if ratio > color_change_ratio_high:
            return ((ratio - color_change_ratio_high) / (1 - color_change_ratio_high) * (255-threshold)+threshold,
                    (ratio-color_change_ratio_high) / (1 - color_change_ratio_high) * (255-threshold)+threshold,
                    (ratio - color_change_ratio_high) / (1 - color_change_ratio_high) * (255-threshold)+threshold)
        if ratio > color_change_ratio_low:
            return ((ratio - color_change_ratio_low) / (color_change_ratio_high - color_change_ratio_low) * threshold,
                    ratio/color_change_ratio_high * threshold,
                    (ratio - color_change_ratio_low) / (color_change_ratio_high - color_change_ratio_low) * threshold)
        return (0, ratio/color_change_ratio_high * threshold, 0)
        # if ratio > 0.5:
        #     return (ratio * 255, 255, ratio * 255)
        # return (0, ratio * 255, 0)

    if style == "blue":
        maxiter = 255
        if n_iter == np.inf or n_iter >= maxiter:
            return (0, 0, 0)
        lim = 255  #100
        if n_iter > lim:
            return (255, 255, 255)

        ratio = n_iter / lim
        color_change_ratio = 0.3
        if ratio > color_change_ratio:
            return ((ratio - color_change_ratio) / (1 - color_change_ratio) *
                    255, (ratio - color_change_ratio) /
                    (1 - color_change_ratio) * 255, ratio * 255)
        return (0, 0, ratio * 255)

    print("style " + style + " undefined")
    return (0, 0, 0)


if __name__ == '__main__':
    width = 3
    height = 3
    center_real = -0.75
    center_imag = 0

    computations = 100e6

    color_scheme = "green"

    lim_real = [center_real - width / 2, center_real + width / 2]
    lim_imag = [center_imag - height / 2, center_imag + height / 2]

    resolution = int(np.sqrt(computations / (width * height)))

    width_pixel = int(width * (resolution - 1) + 1)
    height_pixel = int(height * (resolution - 1) + 1)

    values_real = np.linspace(lim_real[0], lim_real[1], width_pixel)
    values_imag = np.linspace(lim_imag[0], lim_imag[1], height_pixel)
    values = itertools.product(values_real, values_imag)

    #Check if data exists. Compute only if necessary
    parameter_list = [width, height, center_real, center_imag, resolution]
    parameter_hash = hash(tuple(parameter_list))
    filename = os.path.join('data', "{}.json".format(parameter_hash))

    if os.path.isfile(filename):
        with open(filename, "r") as fp:
            _, mandelbrot_iterations_list = json.load(fp)
        print('Data loaded from file. No computation necessary')
    else:
        print('Start computation')
        pool = multiprocessing.Pool()  # 8 processors on 4 cores
        mandelbrot_iterations_list = []
        for result in tqdm.tqdm(pool.imap(mandelbrot.get_iter, values, 1000),
                                total=len(values_real) * len(values_imag)):
            mandelbrot_iterations_list.append(result)
        # mandelbrot_iterations_list = pool.map(mandelbrot.get_iter, values)
        print('Computation ended')

        with open(filename, "w") as fp:
            json.dump([parameter_list, mandelbrot_iterations_list], fp)
        print('Data saved in file')

    print('Converting to image data')
    print('getcolor')
    mandelbrot_image_list = np.array(list(
        map(lambda var: get_color(var, color_scheme),
            mandelbrot_iterations_list)),
                                     dtype=np.uint8)
    print('resize')
    mandelbrot_image = np.resize(mandelbrot_image_list,
                                 (width_pixel, height_pixel, 3))

    print('Create and save image')
    # Use PIL to create an image from the new array of pixels
    new_image = Image.fromarray(mandelbrot_image).transpose(Image.ROTATE_90)
    new_image = new_image.resize((2 * width_pixel, 2 * height_pixel))
    new_image = new_image.resize((width_pixel, height_pixel), Image.ANTIALIAS)
    new_image.save('output/mandelbrot_{}.png'.format(
        time.strftime("%Y%m%d-%H%M%S")))

    print('Done!')
