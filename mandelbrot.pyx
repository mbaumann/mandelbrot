import numpy as np

# import cython
# if cython.compiled:
#     print("Yep, I'm compiled.")
# else:
#     print("Just a lowly interpreted script.")

def get_iter(value):
    """Iterate until converges, diverges or max iteration is reached

    Args:
        param (complex number): Iteration parameter
    """
    cdef:
        int maxiter = 255
        float abs_tol = 1e-6
        float rel_tol = 1e-3
        float complex x = 0
        float complex y = 0
        float complex param = value[0] + 1.j * value[1]

    for i in range(maxiter):
        y = x * x + param
        if y.real*y.real+y.imag+y.imag >= 4:
            return i
        # if np.abs(x - y) < abs_tol or np.abs(x - y) < np.abs(x) * rel_tol:
        #     return np.inf
        x = y
    return maxiter
